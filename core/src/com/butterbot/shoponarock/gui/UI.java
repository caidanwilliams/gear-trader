package com.butterbot.shoponarock.gui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.utils.Align;
import com.butterbot.shoponarock.Main;
import com.butterbot.shoponarock.areas.Home;
import com.butterbot.shoponarock.items.Item;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisList;
import com.kotcrab.vis.ui.widget.VisTable;

public class UI extends Actor {

    private Main gameInstance;
    private Home home;

    private VisTable root, infoTable, resourcesTable;
    private VisLabel credits, shuttles;
    private VisList<String> resourcesValues;

    public UI(Main gameInstance) {
        this.gameInstance = gameInstance;
        this.home = gameInstance.getHome();

        if (!VisUI.isLoaded())
            VisUI.load();

        root = new VisTable();
        root.setFillParent(true);

        infoTable = new VisTable();

        credits = new VisLabel("Credits: [C]0000000");
        credits.setColor(Color.CYAN);

        shuttles = new VisLabel("Shuttles: 00");
        shuttles.setColor(Color.CYAN);

        VisList<String> resourcesText = new VisList<>();
        resourcesText.setItems(
                "Resources:",
                "OXYGEN:",
                "WATER:",
                "COPPER:",
                "IRON:",
                "METEORITE: ",
                "SILVER:",
                "GOLD:",
                "ETHER:"
        );
        resourcesText.setTouchable(Touchable.disabled);

        resourcesValues = new VisList<>();
        resourcesValues.setItems(
                "",
                String.valueOf(home.getGoods().get(Item.OXYGEN)),
                String.valueOf(home.getGoods().get(Item.WATER)),
                String.valueOf(home.getGoods().get(Item.COPPER)),
                String.valueOf(home.getGoods().get(Item.IRON)),
                String.valueOf(home.getGoods().get(Item.METEORITE)),
                String.valueOf(home.getGoods().get(Item.SILVER)),
                String.valueOf(home.getGoods().get(Item.GOLD)),
                String.valueOf(home.getGoods().get(Item.ETHER))
        );
        resourcesValues.setAlignment(Align.right);
        resourcesValues.setTouchable(Touchable.disabled);

        resourcesTable = new VisTable();
        resourcesTable.add(resourcesText);
        resourcesTable.add(resourcesValues);

        infoTable.defaults().left().top().space(10);
        infoTable.add(credits).row();
        infoTable.add(shuttles).row();
        infoTable.add(resourcesTable);

        root.defaults().pad(10).expand();
        root.add(infoTable).left().top();
    }

    @Override
    public void act(float delta) {
        // Center the UI to the camera
        root.setPosition(
                gameInstance.getUiStage().getCamera().position.x - (root.getWidth() / 2),
                gameInstance.getUiStage().getCamera().position.y - (root.getHeight() / 2)
        );

        if (gameInstance.getHome() != null) {
            // Update the value of the credits and shuttles
            credits.setText(String.format("Credits: [C]%07d", gameInstance.getHome().getCredits()));
            shuttles.setText(String.format("Shuttles: %d", gameInstance.getHome().getShuttles().size()));

            // Update the value of resources
            resourcesValues.setItems(
                    "",
                    String.valueOf(home.getGoods().get(Item.OXYGEN)),
                    String.valueOf(home.getGoods().get(Item.WATER)),
                    String.valueOf(home.getGoods().get(Item.COPPER)),
                    String.valueOf(home.getGoods().get(Item.IRON)),
                    String.valueOf(home.getGoods().get(Item.METEORITE)),
                    String.valueOf(home.getGoods().get(Item.SILVER)),
                    String.valueOf(home.getGoods().get(Item.GOLD)),
                    String.valueOf(home.getGoods().get(Item.ETHER))
            );
        }
    }

    public VisTable getRoot() {
        return root;
    }
}
