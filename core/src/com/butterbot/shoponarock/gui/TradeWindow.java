package com.butterbot.shoponarock.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.butterbot.shoponarock.Main;
import com.butterbot.shoponarock.areas.Shop;
import com.butterbot.shoponarock.items.Item;
import com.butterbot.shoponarock.vehicles.Shuttle;
import com.kotcrab.vis.ui.building.utilities.Alignment;
import com.kotcrab.vis.ui.widget.*;
import com.kotcrab.vis.ui.widget.spinner.IntSpinnerModel;
import com.kotcrab.vis.ui.widget.spinner.Spinner;

import java.util.ArrayList;
import java.util.HashMap;

public class TradeWindow extends VisWindow {

    private static final int MAX_COUNT = 10;
    private static final int WINDOW_OFFSET = 20;
    private static int COUNT = 0;

    private TradeWindow selfWindow;
    private Tooltip tooltip;

    private HashMap<Item, Integer> buyList, sellList;

    public TradeWindow(Main gameInstance, Shop shop) {
        super(shop.getName() + " Trade", true);

        this.setVisible(false);     // Set invisible so we don't have weird jitters when trying to create a new window

        COUNT++;                    // Increase the count to keep track of how many windows we have
        if (COUNT > MAX_COUNT) {
            this.close();           // Close the current window
        } else {
            this.setVisible(true);
            buyList = new HashMap<>();
            sellList = new HashMap<>();
            selfWindow = this;
            tooltip = new Tooltip();
            tooltip.setAppearDelayTime(0.3f);
            tooltip.setMouseMoveFadeOut(true);
            tooltip.setTarget(this);
            tooltip.setText("");
            tooltip.detach();
        }

        // Set basic settings for our window
        this.addCloseButton();
//        this.setKeepWithinParent(false);
        this.setKeepWithinStage(false);
        this.setFillParent(false);
        this.setMovable(true);
        this.setResizable(true);
        this.setPosition(
                gameInstance.getUiStage().getCamera().position.x - (gameInstance.getUiStage().getCamera().viewportWidth / 3) + ((COUNT-1) * WINDOW_OFFSET),
                gameInstance.getUiStage().getCamera().position.y + (getHeight() * 0.9f) - ((COUNT-1) * WINDOW_OFFSET)
        );

        VisTable root = new VisTable(true);
        root.setFillParent(true);

        VisLabel totalCostLabel = new VisLabel(String.format("Cost: [C]%04d", 0));
        VisLabel totalProfitLabel = new VisLabel(String.format("Profit: [C]%04d", 0));

        VisTable resourcesTable, spinnersBuyTable, spinnersSellTable;
        resourcesTable = new VisTable(true);
        spinnersBuyTable = new VisTable(true);
        spinnersSellTable = new VisTable(true);

        int[] totalCost = new int[Item.values().length];
        int[] totalProfit = new int[Item.values().length];

        // Loop through all of our items to show spinners for them
        for (Item item : Item.values()) {
            /* Create the resources*/

            VisLabel itemName = new VisLabel(String.format("%s: %04d", item.name(), shop.getGoods().get(item)));
            itemName.setTouchable(Touchable.disabled);
            itemName.setAlignment(Alignment.RIGHT.getAlignment());
            resourcesTable.add(itemName).space(5).height(30).width(130).row();

            /* Create the buy spinners */

            int shopAmt = shop.getGoods().get(item);

            final IntSpinnerModel buyModel = new IntSpinnerModel(0, 0, shopAmt, 1);

            // Integer Spinner Model, this help set base parameters for our spinners
            Spinner buySpinner = new Spinner(String.format("Buy: [C]%03d", shop.totalBuyCost(item, 1)), buyModel);

            if (shopAmt == 0)
                buySpinner.setDisabled(true);

            // Add a listener to the spinner for changes
            buySpinner.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    buyList.put(item, buyModel.getValue());

                    totalCost[item.getRarity()] = shop.totalBuyCost(item, buyModel.getValue());

                    int costSum = 0;
                    for (int credits : totalCost) {
                        costSum += credits;
                    }

                    totalCostLabel.act(Gdx.graphics.getDeltaTime());
                    totalCostLabel.setText(String.format("Cost: [C]%04d", costSum));
                }
            });

            spinnersBuyTable.add(buySpinner).space(5).height(30).row();

            /* Create the sell spinners */

            int homeAmt = gameInstance.getHome().getGoods().get(item);

            final IntSpinnerModel sellModel = new IntSpinnerModel(0, 0, homeAmt, 1);

            // Create a new spinner with the name and model
            Spinner sellSpinner = new Spinner(String.format("Sell: [C]%03d", shop.totalSellCost(item, 1)), sellModel);

            if (homeAmt == 0)
                sellSpinner.setDisabled(true);

            // Add a listener to the spinner for changes
            sellSpinner.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    sellList.put(item, sellModel.getValue());

                    totalProfit[item.getRarity()] = shop.totalSellCost(item, sellModel.getValue());

                    int profitSum = 0;
                    for (int credits : totalProfit) {
                        profitSum += credits;
                    }

                    totalProfitLabel.act(Gdx.graphics.getDeltaTime());
                    totalProfitLabel.setText(String.format("Profit: [C]%04d", profitSum));
                }
            });

            spinnersSellTable.add(sellSpinner).space(5).height(30).row();
        }

        VisTextButton confirmButton = new VisTextButton("Confirm Transaction");
        confirmButton.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (button == Input.Buttons.LEFT) {
                    int totalCost = 0;
                    for (Item item : buyList.keySet()) {
                        if (buyList.get(item) > 0) {
                            totalCost += shop.totalBuyCost(item, buyList.get(item));
                        }
                    }

                    if (totalCost > gameInstance.getHome().getCredits()) {
                        tooltip.detach();
                        tooltip.setText("You do not have enough credits");
                        tooltip.attach();
                        return false;
                    }

                    Shuttle shuttle = gameInstance.getHome().getFreeShuttle();

                    if (shuttle == null) {
                        tooltip.detach();
                        tooltip.setText("All shuttles are in use");
                        tooltip.attach();
                        return false;
                    }

                    // Place all of our buy orders
                    for (Item item : buyList.keySet()) {
                        shuttle.getBuyList().put(item, buyList.get(item));
                    }

                    // Place all of our sell orders
                    for (Item item : sellList.keySet()) {
                        shuttle.getSellList().put(item, sellList.get(item));
                    }

                    shuttle.setDestination(shop);

                    selfWindow.close();
                    return true;
                }
                return false;
            }
        });

        root.add(resourcesTable).padTop(20f);
        root.add(spinnersBuyTable).padTop(20f);
        root.add(spinnersSellTable).padTop(20f).padRight(10f);
        root.row();
        root.add(confirmButton).space(5f);
        root.add(totalCostLabel).space(5f).right();
        root.add(totalProfitLabel).space(5f).padRight(10f).right();

        this.add(root).grow();
        this.pack();

        gameInstance.getUiStage().addActor(this);
    }

    @Override
    protected void close() {
        super.close();

        if (buyList != null && sellList != null) {
            for (Item item : Item.values()) {
                buyList.put(item, 0);
                sellList.put(item, 0);
            }
        }

        if (tooltip != null) {
            tooltip.detach();
        }
        COUNT--;
    }
}
