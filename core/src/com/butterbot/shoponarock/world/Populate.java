package com.butterbot.shoponarock.world;

import com.badlogic.gdx.graphics.Color;
import com.butterbot.shoponarock.Main;
import com.butterbot.shoponarock.areas.Shop;
import com.butterbot.shoponarock.items.Item;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Populate {

    private Main gameInstance;
    private World world;

    private Random rand;

    private List<Shop> allShops = new ArrayList<>();
    private List<String> shopNames;

    public Populate(Main game, World world) {
        this.gameInstance = game;
        this.world = world;

        rand = new Random();

        shopNames = new ArrayList<>(Arrays.asList(
                "Blemflark's Abode",
                "Sammy's Spaceshop",
                "Starway Station",
                "Black Hole Bargains",
                "Milky Way Merchandise",
                "Selah's Scrapyard",
                "Tori's Terrace"
        ));
    }

    public void generateShops() {
        // Randomise the seed to ensure we won't have the same shop population
        rand.setSeed(rand.nextInt() * (world.getSectorSize()*world.getSectorsWide()));

        for (int x = 0; x < world.getSectorsWide(); x++) {
            for (int y = 0; y < world.getSectorsTall(); y++) {
                String name = shopNames.get(rand.nextInt(shopNames.size()));
                Shop shop = new Shop(gameInstance, String.format("%s #%d", name, x+y+rand.nextInt(x+y+1)));
                shop.setSize(10 + rand.nextInt(world.getSectorSize()/8));

                switch (name) {
                    case "Blemflark's Abode":
                        shop.setColor(Color.CORAL);
                        break;
                    case"Sammy's Spaceshop":
                        shop.setColor(Color.CYAN);
                        break;
                    case "Starway Station":
                        shop.setColor(Color.GOLD);
                        break;
                    case "Black Hole Bargains":
                        shop.setColor(Color.LIGHT_GRAY);
                        break;
                    case "Milky Way Merchandise":
                        shop.setColor(Color.PURPLE);
                        break;
                    case "Selah's Scrapyard":
                        shop.setColor(Color.BROWN);
                        break;
                    case "Tori's Terrace":
                        shop.setColor(Color.FOREST);
                        break;
                    default:
                        shop.setColor(Color.WHITE);
                        break;
                }

                for (Item item : Item.values()) {
                    // Set the amount of goods in the shops inv
                    shop.getGoods().put(item, rand.nextInt(shop.getSize() * 2));

                    // Set the buy price of the good off of the amount of goods in the shop
                    shop.getBuyPrices().put(item, shop.determineGoodValue(item, false));

                    // Set the sell price of the good off of the amount of goods in the shop
                    shop.getSellPrices().put(item, shop.determineGoodValue(item, true));
                }

                int ss = world.getSectorSize();
                int shopBound = (shop.getSize()*2); // Make sure the shop doesn't get too close to the edge of the sector
                int xPos = ((x*ss) + rand.nextInt(Math.abs(ss - shopBound) + 1));
                int yPos = ((y*ss) + rand.nextInt(Math.abs(ss - shopBound) + 1));

                shop.setVectorPosition(world.convertToWorldCords(xPos, yPos));

                allShops.add(shop);
                world.addEntity(shop);
            }
        }
    }

}
