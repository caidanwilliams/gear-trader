package com.butterbot.shoponarock.world;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.butterbot.shoponarock.Entity;
import com.butterbot.shoponarock.Main;
import com.butterbot.shoponarock.areas.Home;

import java.util.ArrayList;
import java.util.List;

public class World extends Actor {

    private static final int STARTING_CREDITS = 100;

    private Main gameInstance;
    private Stage stage;
    private OrthographicCamera camera;

    private int sectorsWide,
                sectorsTall,
                sectorSize,
                pixelWidth,
                pixelHeight;

    private Vector2 currPos;

    private Home home;
    private List<Entity> entities;

    public World(Main game, int sectorsWide, int sectorsTall, int sectorSize) {
        this.gameInstance = game;
        this.stage = game.getStage();
        this.camera = game.getCamera();

        setX(-pixelWidth);
        setY(-pixelHeight);
        Vector3 temp = camera.position.cpy().sub(new Vector3(getX(), getY(), 0));
        this.currPos = new Vector2(temp.x, temp.y);

        this.sectorsWide = sectorsWide;
        this.sectorsTall = sectorsTall;
        this.sectorSize = sectorSize;
        this.pixelWidth = sectorsWide * sectorSize;
        this.pixelHeight = sectorsTall * sectorSize;

        this.entities = new ArrayList<>();

        home = new Home(game);
        home.setPosition(pixelWidth/2, pixelHeight/2);
        home.setCredits(STARTING_CREDITS);
        addEntity(home);

        stage.getCamera().position.set(new Vector3((int) home.getX(), (int) home.getY(), 0));

        Populate populate = new Populate(game, this);
        populate.generateShops();
    }

    @Override
    public void act(float delta) {
        super.act(delta);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
    }

    @Override
    public void drawDebug(ShapeRenderer shapes) {
        super.drawDebug(shapes);

        shapes.setColor(Color.LIGHT_GRAY);
        for (int x = 0; x < sectorsWide; x++) {
            for (int y = 0; y < sectorsTall; y++) {
                Vector2 sectorPos = convertToWorldCords(x*sectorSize, y*sectorSize);
                shapes.rect(sectorPos.x, sectorPos.y, sectorSize, sectorSize);
            }
        }
    }

    public Home getHome() {
        return home;
    }

    public List<Entity> getEntities() {
        return entities;
    }

    public void addEntity(Entity entity) {
        entities.add(entity);
        stage.addActor(entity);
    }

    public int getSectorsWide() {
        return sectorsWide;
    }

    public int getSectorsTall() {
        return sectorsTall;
    }

    public int getSectorSize() {
        return sectorSize;
    }

    public int getPixelWidth() {
        return pixelWidth;
    }

    public int getPixelHeight() {
        return pixelHeight;
    }

    public Vector2 convertToWorldCords(int x, int y) {
        return new Vector2(getX() + x, getY() + y);
    }
}
