package com.butterbot.shoponarock;

import com.badlogic.gdx.*;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.butterbot.shoponarock.areas.Home;
import com.butterbot.shoponarock.gui.UI;
import com.butterbot.shoponarock.processors.GameInputProcessor;
import com.butterbot.shoponarock.processors.GuiInputProcessor;
import com.butterbot.shoponarock.screens.GameScreen;
import com.butterbot.shoponarock.screens.StartScreen;
import com.butterbot.shoponarock.util.ContentLoader;
import com.butterbot.shoponarock.world.World;

import java.util.List;

public class Main extends Game {

    private SpriteBatch batch;
    private ShapeRenderer shapeRenderer;
    private BitmapFont font;

	private OrthographicCamera camera;
	private Stage stage, uiStage;

	private StartScreen startScreen;
	private GameScreen gameScreen;

	private UI ui;
	private World world;

	@Override
	public void create () {
        // Enable debug logging
        Gdx.app.setLogLevel(Application.LOG_DEBUG);

        shapeRenderer = new ShapeRenderer();
        shapeRenderer.setAutoShapeType(true);
        shapeRenderer.setColor(Color.WHITE);

        font = ContentLoader.getFont("assets/fonts/RobotoSlab-Regular.ttf", Color.WHITE, 14);

        /*Shop shop1 = new Shop("Traxor 3", shapeRenderer);
        shop1.setSize(20);
        shop1.setPosition(300, 400);
        shop1.getGoods().put(Item.OXYGEN, 5);

        Shop shop2 = new Shop("Traxor 6", shapeRenderer);
        shop2.setSize(20);
        shop2.setPosition(600, 300);
        shop2.getGoods().put(Item.COPPER, 5);

        Shuttle shuttle = new Shuttle("SS Satori", shapeRenderer, home);
        shuttle.setPosition(480, 40);
        home.getShuttles().add(shuttle);*/

        batch = new SpriteBatch();

        camera = new OrthographicCamera();
        camera.setToOrtho(false);

        stage = new Stage(new ScreenViewport(camera), batch);

        /*addEntity(home);
        addEntity(shop1);
        addEntity(shop2);
        addEntity(shuttle);*/

        uiStage = new Stage(new ScreenViewport(), batch);

        uiStage.getCamera().viewportWidth = camera.viewportWidth;
        uiStage.getCamera().viewportHeight = camera.viewportHeight;

        InputMultiplexer multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(uiStage);
        multiplexer.addProcessor(stage);
        multiplexer.addProcessor(new GuiInputProcessor(this));
        multiplexer.addProcessor(new GameInputProcessor(this));
        Gdx.input.setInputProcessor(multiplexer);

        this.world = new World(this, 25, 20, 516);
        this.ui = new UI(this);

        this.setScreen(new StartScreen(this));
    }

	@Override
	public void render() {
	    super.render();
	    // Clear screen with a very nice deep blue
		Gdx.gl.glClearColor(0.1f, 0.1f, 0.15f, 1f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		System.out.println("FPS: " + Gdx.graphics.getFramesPerSecond());
//		System.out.println("Screen: " + getScreen());

		screen.render(Gdx.graphics.getDeltaTime());

        // Random debug stuff
        if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
            Gdx.app.exit();
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.O)) {
            stage.setDebugAll(!stage.isDebugAll());
            uiStage.setDebugAll(!uiStage.isDebugAll());
        }
	}

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, false);
        uiStage.getViewport().update(width, height, true);
    }

    @Override
	public void dispose () {
		batch.dispose();
	}

    public Stage getStage() {
        return stage;
    }

    public Stage getUiStage() {
        return uiStage;
    }

    public UI getUi() {
        return ui;
    }

    public World getWorld() {
        return world;
    }

    public OrthographicCamera getCamera() {
        return camera;
    }

    public List<Entity> getEntities() {
        return world.getEntities();
    }

    public void addEntity(Entity entity) {
	    world.addEntity(entity);
    }

    public Home getHome() {
        return world.getHome();
    }

    public SpriteBatch getBatch() {
        return batch;
    }

    public ShapeRenderer getShapeRenderer() {
        return shapeRenderer;
    }

    public BitmapFont getFont() {
        return font;
    }

    public void setFontSize(int size) {
        font  = ContentLoader.getFont("assets/fonts/RobotoSlab-Regular.ttf", Color.WHITE, size);
        world.getEntities().forEach(entity -> entity.setFont(getFont()));
    }
}
