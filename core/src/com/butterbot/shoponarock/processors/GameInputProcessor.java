package com.butterbot.shoponarock.processors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.butterbot.shoponarock.Entity;
import com.butterbot.shoponarock.Main;
import com.butterbot.shoponarock.areas.Shop;
import com.butterbot.shoponarock.gui.TradeWindow;

public class GameInputProcessor implements InputProcessor {

    private Main gameInstance;
    private OrthographicCamera camera;

    private Vector3 touchPos;
    private Vector2 lastTouchPos;

    public GameInputProcessor(Main gameInstance) {
        this.gameInstance = gameInstance;
        this.camera = gameInstance.getCamera();
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (button == Input.Buttons.LEFT) {
            // Unproject our screen cords to our game world cords
            touchPos = gameInstance.getCamera().unproject(new Vector3(screenX, screenY, 0));


            for (Entity entity : gameInstance.getEntities()) {
                // Check if we clicked on a valid shop
                if (entity instanceof Shop && entityWithin(entity, touchPos.x, touchPos.y)) {
//                        gameInstance.getHome().getShuttle().setDestination((Area) entity);
                    new TradeWindow(gameInstance, (Shop) entity);
                    return true;
                }
            }
        }

        if (button == Input.Buttons.RIGHT) {
            // Unproject our screen cords to our game world cords
            lastTouchPos = new Vector2(-screenX * camera.zoom, screenY * camera.zoom);
            return true;
        }

        return false;
    }

    private boolean entityWithin(Entity entity, float x, float y) {
            return x > entity.getX() && x < entity.getX() + entity.getWidth() &&
                    y > entity.getY() && y < entity.getY() + entity.getHeight();
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if (Gdx.input.isButtonPressed(Input.Buttons.RIGHT)) {
            // Create a new vector for the touched position
            Vector2 newTouchPos = new Vector2(-screenX * camera.zoom, screenY * camera.zoom);

            // Move the camera by the distance from the last touched position
            camera.translate(newTouchPos.cpy().sub(lastTouchPos));

            if ((camera.position.x + camera.viewportWidth) > gameInstance.getWorld().getPixelWidth() + 300) {
                camera.position.set(new Vector2(gameInstance.getWorld().getPixelWidth() - camera.viewportWidth + 300, camera.position.y), camera.position.z);
            }
            else if ((camera.position.x - camera.viewportWidth) < gameInstance.getWorld().getX() - 300) {
                camera.position.set(new Vector2(gameInstance.getWorld().getX() + camera.viewportWidth - 300, camera.position.y), camera.position.z);
            }

            if ((camera.position.y + camera.viewportHeight) > gameInstance.getWorld().getPixelHeight() + 300) {
                camera.position.set(new Vector2(camera.position.x, gameInstance.getWorld().getPixelHeight() - camera.viewportHeight + 300), camera.position.z);
            }
            else if ((camera.position.y - camera.viewportHeight) < gameInstance.getWorld().getY() - 300) {
                camera.position.set(new Vector2(camera.position.x, gameInstance.getWorld().getY() + camera.viewportHeight - 300), camera.position.z);
            }

            lastTouchPos = newTouchPos;
            return true;
        }

        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        camera.zoom += amount * 0.1f;

        if (camera.zoom < 0.5f) {
            camera.zoom = 0.5f;
            return false;
        }

        if (camera.zoom > 2f)
            camera.zoom = 2f;

        gameInstance.getUi().getRoot().setScale(camera.zoom);

        int fontSize = Math.round(14 * camera.zoom);
        if (fontSize < 14) {
            gameInstance.setFontSize(14);
        } else {
            gameInstance.setFontSize(fontSize);
        }

        return true;
    }
}
