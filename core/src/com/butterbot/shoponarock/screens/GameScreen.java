package com.butterbot.shoponarock.screens;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.butterbot.shoponarock.Main;
import com.butterbot.shoponarock.world.World;

public class GameScreen implements Screen {

    private Main gameInstance;
    private Stage stage, uiStage;
    private SpriteBatch batch;
    private OrthographicCamera camera;
    private ShapeRenderer shapeRenderer;

    private World world;

    public GameScreen(Main game) {
        this.gameInstance = game;
        this.stage = game.getStage();
        this.uiStage = game.getUiStage();
        this.batch = game.getBatch();
        this.camera = game.getCamera();
        this.shapeRenderer = game.getShapeRenderer();
        this.world = game.getWorld();
    }

    @Override
    public void show() {
        stage.addActor(world);
        uiStage.addActor(gameInstance.getUi());
        uiStage.addActor(gameInstance.getUi().getRoot());
    }

    @Override
    public void render(float delta) {
        // Handle the stage
        stage.act();
        uiStage.act();

        // Set batch and projection matrices for better screen scaling
        batch.setProjectionMatrix(camera.combined);
        shapeRenderer.setProjectionMatrix(camera.combined);

        shapeRenderer.begin();
        stage.draw();
        shapeRenderer.end();

        // Draw each entities "HUD" which is really just their name
        batch.begin();
        world.getEntities().forEach(entity -> entity.drawHUD(batch));
        batch.end();

        // Handle the UI last so we don't get any element jitters
        uiStage.draw();
    }

    @Override
    public void resize(int width, int height) {
        gameInstance.resize(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
    }

    @Override
    public void dispose() {
        stage.dispose();
        uiStage.dispose();
        shapeRenderer.dispose();
    }
}
