package com.butterbot.shoponarock.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.butterbot.shoponarock.Main;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextButton;

import java.util.Random;

public class StartScreen implements Screen {

    private Main gameInstance;
    private Stage uiStage;
    private ShapeRenderer renderer;

    private VisTable main;

    private Random rand;
    private int x, y, radius;

    public StartScreen(Main game) {
        this.gameInstance = game;
        this.uiStage = game.getUiStage();
        this.renderer = game.getShapeRenderer();

        this.rand = new Random();
    }

    private VisTable buildUI() {
        if (!VisUI.isLoaded())
            VisUI.load();

        VisTable root = new VisTable(true);
        root.setFillParent(true);

        VisLabel titleLabel = new VisLabel("Shop on a Rock");
        titleLabel.setFontScale(6);
        titleLabel.setColor(Color.WHITE);

        VisTextButton playButton = new VisTextButton("Play");
        playButton.addListener(new ClickListener(){
            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                super.enter(event, x, y, pointer, fromActor);
                playButton.setWidth(200);
            }

            @Override
            public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
                super.exit(event, x, y, pointer, toActor);
                playButton.setWidth(150);
            }

            @Override
            public void clicked(InputEvent event, float x, float y) {
                gameInstance.setScreen(new GameScreen(gameInstance));
            }
        });

        VisTextButton exitButton = new VisTextButton("Exit");
        exitButton.addListener(new ClickListener(){
            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                super.enter(event, x, y, pointer, fromActor);
                exitButton.setWidth(200);
            }

            @Override
            public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
                super.exit(event, x, y, pointer, toActor);
                exitButton.setWidth(150);
            }

            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.exit();
            }
        });

        root.add(titleLabel).center().space(30f);
        root.row();
        root.defaults().left().width(150).space(30f);
        root.add(playButton).row();
        root.add(exitButton).row();

        return root;
    }

    @Override
    public void show() {
        main = buildUI();
        uiStage.addActor(main);

        x = rand.nextInt(Gdx.graphics.getWidth());
        y = rand.nextInt(Gdx.graphics.getHeight());
        radius = 0;
    }

    @Override
    public void render(float delta) {
        uiStage.act(delta);

        if (x + radius < Gdx.graphics.getWidth() + 350 || y + radius < Gdx.graphics.getHeight() ||
                x - radius > -350 || y - radius > 0) {
            radius += 2;
        } else {
            x = rand.nextInt(Gdx.graphics.getWidth());
            y = rand.nextInt(Gdx.graphics.getHeight());
            radius = 0;
        }

        renderer.begin();

        renderer.setColor(Color.RED);
        renderer.circle(x, y, radius + 45);

        renderer.setColor(Color.ORANGE);
        renderer.circle(x, y, radius + 30);

        renderer.setColor(Color.YELLOW);
        renderer.circle(x, y, radius + 15);

        renderer.setColor(Color.GREEN);
        renderer.circle(x, y, radius);

        renderer.setColor(Color.BLUE);
        renderer.circle(x, y, radius - 15);

        renderer.setColor(Color.PURPLE);
        renderer.circle(x, y, radius - 30);

        renderer.setColor(Color.PINK);
        renderer.circle(x, y, radius - 45);

        renderer.end();

        uiStage.draw();
    }

    @Override
    public void resize(int width, int height) {
        gameInstance.resize(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        main.remove();
    }

    @Override
    public void dispose() {
        VisUI.dispose();

        if (uiStage.getActors().contains(main, false)) {
            main.remove();
        }
    }
}
