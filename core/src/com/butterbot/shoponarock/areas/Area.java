package com.butterbot.shoponarock.areas;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.butterbot.shoponarock.Entity;
import com.butterbot.shoponarock.Main;
import com.butterbot.shoponarock.items.Item;

import java.util.HashMap;

public class Area extends Entity {

    protected HashMap<Item, Integer> goods = new HashMap<>();

    private int size = 10;

    public Area(Main game, String name) {
        super(game, name);

        setSize(size*2, size*2);

        for (Item item : Item.values()) {
            getGoods().put(item, 0);
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        renderer.setColor(getColor());
        renderer.ellipse(getX(), getY(), getWidth(), getHeight());
    }

    @Override
    public void drawHUD(Batch batch) {
        font.draw(
                batch,
                getName(),
                (getX() + getOriginX()) - (layout.width / 2),
                (getY() + getOriginY()) - (layout.height + size)
        );
    }

    public HashMap<Item, Integer> getGoods() {
        return goods;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
        setSize(size*2, size*2);
    }
}
