package com.butterbot.shoponarock.areas;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.butterbot.shoponarock.Main;
import com.butterbot.shoponarock.vehicles.Shuttle;

import java.util.ArrayList;

public class Home extends Area {

    private int maxShuttles = 3;
    private ArrayList<Shuttle> shuttles = new ArrayList<>();

    public Home(Main gameInstance) {
        super(gameInstance, "Shop on a Rock");
        setColor(Color.RED);
    }

    public ArrayList<Shuttle> getShuttles() {
        // Init shuttles list if there are none currently
        for (int i = shuttles.size(); i < maxShuttles; i++) createShuttle();

        return shuttles;
    }

    public Shuttle getFreeShuttle() {
        for (Shuttle shuttle : shuttles) {
            if (!shuttle.isInRoute())
                return shuttle;
        }

        if (shuttles.size() < maxShuttles) {
            return createShuttle();
        } else {
            return null;
        }
    }

    public Shuttle createShuttle() {
        Shuttle shuttle = new Shuttle(gameInstance,"Shuttle");
        shuttle.setVectorPosition(this.getVectorPosition());
        shuttles.add(shuttle);
        shuttle.setName(String.format("Shuttle %d", shuttles.indexOf(shuttle)));

        gameInstance.addEntity(shuttle);
        return shuttle;
    }

    public int getMaxShuttles() {
        return maxShuttles;
    }

    public void setMaxShuttles(int maxShuttles) {
        this.maxShuttles = maxShuttles;
    }
}
