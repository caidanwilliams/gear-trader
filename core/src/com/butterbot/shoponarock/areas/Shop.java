package com.butterbot.shoponarock.areas;

import com.butterbot.shoponarock.Main;
import com.butterbot.shoponarock.items.Item;

import java.util.HashMap;

public class Shop extends Area {

    private HashMap<Item, Integer> sellPrices = new HashMap<>();
    private HashMap<Item, Integer> buyPrices = new HashMap<>();

    public Shop(Main game, String name) {
        super(game, name);

        for (Item item : Item.values()) {
            sellPrices.put(item, 0);
        }

        for (Item item : Item.values()) {
            buyPrices.put(item, 0);
        }
    }

    // This shop buy's a good from the shuttle
    public int sellGoodTo(Item item, int amount) {
        if (goods.containsKey(item)) {
            goods.put(item, goods.get(item) + amount);
        } else {
            goods.put(item, amount);
        }

        int credits = totalSellCost(item, amount);

        updateBuyPrices();

        return credits;
    }

    // This shop sells a good to the shuttle
    public int buyGoodFrom(Item item, int amount) {
        // Make sure the shop has the item, and that we can sell the appropriate amount
        if (goods.containsKey(item) && goods.get(item) - amount >= 0) {
            int price = totalBuyCost(item, amount);
            goods.put(item, goods.get(item) - amount);
            this.addCredits(price);

            updateSellPrices();

            return price; // Transaction successful
        }

        return 0;
    }

    public void updateBuyPrices() {
        for (Item item : Item.values()) {
            buyPrices.put(item, determineGoodValue(item, false));
        }
    }

    public void updateSellPrices() {
        for (Item item : Item.values()) {
            sellPrices.put(item, determineGoodValue(item, true));
        }
    }

    public int determineGoodValue(Item item, boolean selling) {
        int value;
        if (!selling) {
            value = (int) Math.ceil((item.getPrice()*goods.get(item))-getSize());
        } else {
            value = (int) Math.ceil(item.getPrice()/(goods.get(item)+1))*2;
        }

        return value < 0 ? 0 : value;
    }

    public int totalBuyCost(Item item, int amount) {
        int price = (item.getPrice() + sellPrices.get(item)) * amount;
        return price < 0 ? 0 : price;
    }

    public int totalSellCost(Item item, int amount) {
        return (item.getPrice() + buyPrices.get(item)) * amount;
    }

    public HashMap<Item, Integer> getSellPrices() {
        return sellPrices;
    }

    public HashMap<Item, Integer> getBuyPrices() {
        return buyPrices;
    }

}
