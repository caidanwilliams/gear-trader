package com.butterbot.shoponarock.items;

public enum Item {

    OXYGEN      (0, 10),
    WATER       (1, 20),
    COPPER      (2, 40),
    IRON        (3, 80),
    METEORITE   (4, 60),
    SILVER      (5, 120),
    GOLD        (6, 140),
    ETHER       (7, 150);

    private int rarity;
    private int price;

    Item(int rarity, int price) {
        this.rarity = rarity;
        this.price = price;
    }

    public int getRarity() {
        return rarity;
    }

    public int getPrice() {
        return price;
    }
}
