package com.butterbot.shoponarock.items;

public class ItemOrder {

    private Item item;
    private int amount;

    public ItemOrder(Item item, int amount) {
        this.item = item;
        this.amount = amount;
    }

    public Item getItem() {
        return item;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int itemAmount) {
        this.amount = itemAmount;
    }
}
