package com.butterbot.shoponarock.items;

import com.butterbot.shoponarock.areas.Area;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

public class Exchange<K,V> extends HashMap<K,V> {

    private Area destination;

    public Exchange() {
        super();
    }

    public Area getDestination() {
        return destination;
    }

    public void setDestination(Area destination) {
        this.destination = destination;
    }

    @Override
    public int size() {
        return super.size();
    }

    @Override
    public boolean isEmpty() {
        return super.isEmpty();
    }

    @Override
    public V get(Object key) {
        return super.get(key);
    }

    @Override
    public boolean containsKey(Object key) {
        return super.containsKey(key);
    }

    @Override
    public V put(K key, V value) {
        return super.put(key, value);
    }

    @Override
    public V remove(Object key) {
        return super.remove(key);
    }

    @Override
    public Set<K> keySet() {
        return super.keySet();
    }

    @Override
    public Collection<V> values() {
        return super.values();
    }

    @Override
    public boolean remove(Object key, Object value) {
        return super.remove(key, value);
    }

    @Override
    public V replace(K key, V value) {
        return super.replace(key, value);
    }
}
