package com.butterbot.shoponarock;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.butterbot.shoponarock.util.ContentLoader;

public abstract class Entity extends Actor {

    protected Main gameInstance;
    protected ShapeRenderer renderer;
    protected BitmapFont font;
    protected GlyphLayout layout;

    protected Vector2 currPos;

    protected int credits;

    public Entity(Main game, String name) {
        this.setName(name);

        gameInstance = game;
        renderer = game.getShapeRenderer();

        font = game.getFont();
        layout = new GlyphLayout(font, name);

        if (currPos == null) {
            currPos = new Vector2();
        }
    }

    @Override
    public void act(float delta) {
        super.act(delta);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        renderer.setColor(this.getColor());
    }

    public void drawHUD(Batch batch) {
        if (isVisible()) {
            font.draw(
                    batch,
                    getName(),
                    (getX() + getOriginX()) - layout.width/2,
                    (getY() + getOriginY()) - (layout.height * 2)
            );
        }
    }

    @Override
    public void drawDebug(ShapeRenderer shapes) {
        super.drawDebug(shapes);

        // Purple dot for the origin
        shapes.setColor(Color.MAGENTA);
        shapes.circle(getX() + getOriginX(), getY() + getOriginY(), 3);

        // Yellow rect from the x, y to the origin that is rotated
        shapes.setColor(Color.YELLOW);
        shapes.rect(getX(), getY(), getOriginX(), getOriginY(), getOriginX(), getOriginY(), 1f, 1f, getRotation());
    }

    public void setShapeRenderer(ShapeRenderer renderer) {
        this.renderer = renderer;
    }

    public ShapeRenderer getShapeRenderer() {
        return renderer;
    }

    public void setFontColor(Color color) {
        font.setColor(color);
    }

    public void setFont(BitmapFont font) {
        this.font = font;
        this.layout = new GlyphLayout(font, getName());
    }

    @Override
    public void setSize(float width, float height) {
        super.setSize(width, height);
        setOrigin(width/2, height/2);
    }

    public int getCredits() {
        return credits;
    }

    public void setCredits(int credits) {
        this.credits = credits;
    }

    public void addCredits(int credits) {
        this.credits += credits;
    }

    public int removeCredits(int credits) {
        this.credits -= credits;
        return credits;
    }

    @Override
    public void setPosition(float x, float y) {
        super.setPosition(x, y);
        currPos.set(x, y);
    }

    public Vector2 getVectorPosition() {
        return currPos;
    }

    public void setVectorPosition(Vector2 vector) {
        setPosition(vector.x, vector.y);
    }
}

