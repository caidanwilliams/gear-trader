package com.butterbot.shoponarock.vehicles;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool;
import com.butterbot.shoponarock.Entity;
import com.butterbot.shoponarock.Main;
import com.butterbot.shoponarock.areas.Area;
import com.butterbot.shoponarock.areas.Home;

public abstract class Ship extends Entity implements Pool.Poolable {

    private Home home;
    protected boolean isHome;
    private Area destination;

    private float speed = 100f;
    private long ticksLived = 0;
    private long timeArrived = 0;

    private Vector2 destPos;
    private Vector2 prevDest;

    private float distance;
    private Vector2 direction;

    protected boolean exchangingGoods = false;
    protected boolean inRoute = false;

    public Ship(Main game, String name) {
        super(game, name);
        this.setHome(game.getHome());
        this.isHome = true;
        this.setVisible(false);
    }

    @Override
    public void reset() {

    }

    @Override
    public void act(float delta) {
        super.act(delta);

        if (System.currentTimeMillis() - timeArrived < 1000)
            return;

        if (destination != null && !exchangingGoods) {
            setVectorPosition(currPos.mulAdd(direction, speed * delta)); // Move towards the desired area

            // Stop when our distance from home has exceeded the destination distance
            if (currPos.cpy().sub(prevDest).len() >= distance) {
                setVectorPosition(destPos);
                if (destination != home) {
                    arrived(); // Sell all products and possibly buy some products and head home
                    prevDest = destPos;
                    setDestination(home);
                } else if (destination == home) {
                    isHome = true;
                    arrived(); // Gather gold and get ready to send out again
                    setDestination(null);
                }
            }
        }
        ticksLived++;
    }

    protected void arrived() {
        inRoute = false;
        this.setVisible(!isHome);
        this.timeArrived = System.currentTimeMillis();
    }

    public Area getDestination() {
        return destination;
    }

    public void setDestination(Area destination) {
        this.destination = destination;

        if (destination != null) {
            destPos = new Vector2(
                    destination.getX() + destination.getOriginX() - getOriginX(),
                    destination.getY() + destination.getOriginY() - getOriginY()
            );

            if (currPos == null) {
                currPos = new Vector2(getX() + getOriginX(), getY() + getOriginY());
            }

            if (prevDest == null && destination != home) {
                prevDest = new Vector2(home.getX() + home.getOriginX(), home.getY() + home.getOriginY());
                isHome = false;
            }

            distance = destPos.cpy().sub(currPos).len();
            direction = destPos.cpy().sub(currPos).nor();
            setRotation(direction.angle());

            inRoute = true;
        } else {
            destPos = null;
            prevDest = null;
            distance = 0f;
            direction = null;
            setRotation(0);
        }

        this.setVisible(!isHome);
    }

    public Home getHome() {
        return home;
    }

    public void setHome(Home home) {
        this.home = home;
    }

    public boolean isInRoute() {
        return inRoute;
    }

    public long getTicksLived() {
        return ticksLived;
    }

    public void setTicksLived(long ticksLived) {
        this.ticksLived = ticksLived;
    }
}
