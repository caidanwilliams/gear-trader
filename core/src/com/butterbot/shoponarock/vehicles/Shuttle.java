package com.butterbot.shoponarock.vehicles;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.butterbot.shoponarock.Main;
import com.butterbot.shoponarock.areas.Area;
import com.butterbot.shoponarock.areas.Shop;
import com.butterbot.shoponarock.items.Exchange;
import com.butterbot.shoponarock.items.Item;

import java.util.HashMap;

public class Shuttle extends Ship {

    // <Item> to sell and buy, <Integer> the amount to sell
    private Exchange<Item, Integer> sellList = new Exchange<>();
    private Exchange<Item, Integer> buyList = new Exchange<>();

    // <Item> current item in cargo, <Integer> the amount of the item
    private HashMap<Item, Integer> cargo = new HashMap<>();

    public Shuttle(Main game, String name) {
        super(game, name);

        setSize(30, 20);

        for (Item item : Item.values()) {
            cargo.put(item, 0);
            sellList.put(item, 0);
            buyList.put(item, 0);
        }
    }

    @Override
    protected void arrived() {
        super.arrived();

        if (isHome) {
            this.unloadCargo();
            return;
        } else {
            // We just arrived at another shop, now make sure we don't leave before selling everything
            exchangingGoods = true;
        }

        // Assign the dest to create more efficient local variables
        Shop destShop = (Shop) getDestination();
        sellList.setDestination(destShop);
        buyList.setDestination(destShop);

        for (Item good : sellList.keySet()) {
            if (cargo.containsKey(good)) {
                int holdAmt = cargo.get(good);
                int sellAmt = sellList.get(good);

                // Sell the goods and gain profit
                if (sellAmt <= holdAmt) {
                    this.addCredits(destShop.sellGoodTo(good, sellAmt));
                    cargo.put(good, holdAmt - sellAmt);
                } else {
                    this.addCredits(destShop.sellGoodTo(good, holdAmt));
                    cargo.put(good, 0);
                }
            }
        }

        // See what we might need to buy
        for (Item good : buyList.keySet()) {
            if (destShop.getGoods().containsKey(good)) {
                int holdAmt = cargo.get(good);
                int buyAmt = buyList.get(good);

                // Make sure we have enough credits
                if (credits >= destShop.totalBuyCost(good, buyAmt)) {
                    // Determine the cost of buying the total goods
                    int cost = destShop.buyGoodFrom(good, buyAmt);

                    // If cost is 0 then the shop couldn't sell us any of the desired resource, so skip it
                    if (cost > 0) {
                        cargo.put(good, holdAmt + buyAmt);
                        this.removeCredits(cost);
                    }
                }
            }
        }

        // Now we can leave
        exchangingGoods = false;
    }

    @Override
    public void setDestination(Area destination) {
        super.setDestination(destination);
        if (destination != getHome() && destination != null) {
            loadCargo();
        }
    }

    private void loadCargo() {
        HashMap<Item, Integer> homeGoods = getHome().getGoods();

        for (Item item : sellList.keySet()) {
            if (homeGoods.containsKey(item) && homeGoods.get(item) >= sellList.get(item)) {
                cargo.put(item, sellList.get(item));
                homeGoods.put(item, homeGoods.get(item) - sellList.get(item));
            }
        }

        for (Item item : buyList.keySet()) {
            if (buyList.get(item) > 0) {
                this.addCredits(
                        getHome().removeCredits(
                                ((Shop) getDestination()).totalBuyCost(item, buyList.get(item))
                        )
                );
            }
        }
    }

    private void unloadCargo() {
        HashMap<Item, Integer> homeGoods = getHome().getGoods();

        for (Item item : cargo.keySet()) {
            if (cargo.get(item) > 0) {
                homeGoods.put(item, homeGoods.get(item) + cargo.get(item));
                cargo.put(item, 0);
            }
        }

        getHome().addCredits(this.removeCredits(this.getCredits()));

        buyList.clear();
        sellList.clear();
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        renderer.ellipse(getX(), getY(), getWidth(), getHeight(), getRotation());
    }

    public Exchange<Item, Integer> getSellList() {
        return sellList;
    }

    public Exchange<Item, Integer> getBuyList() {
        return buyList;
    }

    public HashMap<Item, Integer> getCargo() {
        return cargo;
    }
}
