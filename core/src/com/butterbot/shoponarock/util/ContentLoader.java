package com.butterbot.shoponarock.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import java.util.HashMap;
import java.util.Map;

public class ContentLoader {
    private static Map<String, Skin> skinCache = new HashMap<>();
    private static Map<String, BitmapFont> fontCache = new HashMap<>();

    public static Skin getSkin(String jsonFileName, String atlasFileName) {
        if(!skinCache.containsKey(jsonFileName)) {
            skinCache.put(jsonFileName, new Skin(
                    Gdx.files.internal(jsonFileName),
                    new TextureAtlas(Gdx.files.internal(atlasFileName))
            ));
        }
        return skinCache.get(jsonFileName);
    }

    public static BitmapFont getFont(String filename, Color color, int size) {
        String id = filename+color+size;
        if(!fontCache.containsKey(id)) {
            FreeTypeFontParameter parameter = new FreeTypeFontParameter();
            parameter.color = color;    // font color
            parameter.size = size;      // font size
            fontCache.put(id, new FreeTypeFontGenerator(Gdx.files.internal(filename)).generateFont(parameter));
        }

        return fontCache.get(id);
    }
}
