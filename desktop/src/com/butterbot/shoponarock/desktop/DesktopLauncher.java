package com.butterbot.shoponarock.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.butterbot.shoponarock.Main;

import java.awt.*;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		// Add to the config the game title, screen width, and screen height.
		config.title = "Shop on a Rock";
		Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
		config.width = size.width;
		config.height = size.height;
//		config.fullscreen = true;
        config.resizable = true;

		new LwjglApplication(new Main(), config);
	}
}
